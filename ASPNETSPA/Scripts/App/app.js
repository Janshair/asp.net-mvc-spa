﻿/// <reference path="../angular.min.js" />

var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'Files/Home.html',
            controller: 'appController'
        }).
        when('/about', {
             templateUrl: 'Files/About.html',
             controller: 'appController'
        }).
        when('/contact', {
              templateUrl: 'Files/contact.html',
              controller: 'appController'
       }).
       otherwise({
               redirectTo: '/'
       });
}]);

app.controller('appController', function ($scope) {

});